// Penelopean Robotics Copyright (C) 2019 FoAM Kernow
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include <nrf24l01.h>
#include <avr/wdt.h>
#include "I2CSlave.h"

uint8_t to_address[5] = { 0xa7, 0xa7, 0xa7, 0xa7, 0x08 };
uint8_t msg[32];

#define RADIO_ID 0xA7A7A7A7AA

#define I2C_ADDR 0x10

volatile uint8_t state;
volatile uint8_t cmd;
volatile uint8_t pos;
volatile uint8_t transmit;
volatile uint8_t ping;

enum i2c_state {
  STATE_COMMAND,
  STATE_ARGUMENT
};

#define CMD_PING         0x00
#define CMD_SET_RADIO_ID 0x01
#define CMD_FILL_DATA    0x02
#define CMD_TRANSMIT     0x03

void i2c_received(uint8_t data) {
  switch (state) {

  case STATE_COMMAND: {
    cmd = data;
    state = STATE_ARGUMENT;
    pos = 0;

    if (cmd == CMD_TRANSMIT) {
      transmit = 1;
      state = STATE_COMMAND;
    }

    if (cmd == CMD_PING) {
      ping = 1;
      state = STATE_COMMAND;
    }

  } break;

  case STATE_ARGUMENT: {
    switch (cmd) {

    case CMD_SET_RADIO_ID: {      
      to_address[4] = data;
      long long unsigned int *addr = (long long unsigned int *)to_address;
      nRF24L01p_config_pipe(nRF24L01p_PIPE_0, *addr, 32);       
      state = STATE_COMMAND;
    } break;
    
    case CMD_FILL_DATA: {
      msg[pos++] = data;
      if (pos>=32) { 
        state = STATE_COMMAND;
      }
    } break;

    }
        
  } break;    
  }
}

void i2c_requested() {
  I2C_transmitByte(0x99);
}

// watchdog interrupt
ISR(WDT_vect) {
  wdt_disable(); // disable watchdog
}

int main (void) {
  // set up the radio first
  _delay_us(nRF24L01p_TIMING_INITIAL_US);
  nRF24L01p_init(0,0);
  nRF24L01p_config_pipe(nRF24L01p_PIPE_0, 0xA7A7A7A708, 32); 
  nRF24L01p_config_pipe(nRF24L01p_PIPE_1, RADIO_ID, 32); 
  nRF24L01p_config_channel(100);
  nRF24L01p_config_auto_ack(nRF24L01p_MASK_EN_AA_ENAA_ALL, TRUE);
  nRF24L01p_config_transceiver_mode(nRF24L01p_VALUE_CONFIG_PRIM_TX);
  nRF24L01p_enable_ack_payload();
  char ack_payload[] = "Ack Payload\n";
  nRF24L01p_ack_payload(0,(const byte *)ack_payload,12);

   
  I2C_setCallbacks(i2c_received, i2c_requested);
  I2C_init(I2C_ADDR);
  
  sei();   

  memset(&msg, 0, 32);

  // set default message 
  msg[0]='P';
  state == STATE_COMMAND;
  
  // switch on indicator
  DDRB |= 0x01;

  for (int i=0; i<20; i++) {
    PORTB |= 0x01;
    _delay_ms(50);
    PORTB &= ~0x01;
    _delay_ms(50);
  }
  
  for(;;) {    
        
    // message sent indicator flash
    if (transmit && !nRF24L01p_write((void *)&msg, 32, nRF24L01p_PIPE_0)) {        
      PORTB |= 0x01;
      _delay_ms(100);
      PORTB &= ~0x01;
      transmit = 0;
    }

    if (ping) {        
      PORTB |= 0x01;
      _delay_ms(100);
      PORTB &= ~0x01;
      _delay_ms(100);
      PORTB |= 0x01;
      _delay_ms(100);
      PORTB &= ~0x01;
      ping = 0;
    }
    
    _delay_ms(100);
  }

}
