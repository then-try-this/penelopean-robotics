import serial
import time
import json
import transmit

s = serial.Serial("/dev/ttyACM0", 115200, timeout=None)

id_to_knot = {
    1: "-/-",
    2: "b",
    3: "-///-",
    4: "-/-",
    5: "-/-",
    6: "-//-",
    8: "-//-",
    9: "-////-",
    16: "-/--//-",
    17: "fb",
    18: "m",
    19: "-//-",
    21: "-////-",
}

knot_to_instr = {
    "m": "sequence_pattern_middle",
    "fb": "sequence_pattern_front",
}

knot_to_pat = {
    "--": "\"0\"",
    "-/-": "\"a\"",
    "-//-": "\"A\""
}

instr_to_context = {
    "sequence_pattern_middle": knot_to_pat,
    "sequence_pattern_front": knot_to_pat,
    "sequence_pattern_back": knot_to_pat,
}


# {'desc': [{'desc': [], 'id': 5, 'order': 2}], 'id': 1, 'order': 0}


def knot_to(k, lookup, default):
    if not k in lookup:
        return default
    else:
        return lookup[k]
        
def pendant_to_python(k, depth, context):
    if k['id'] in id_to_knot:
        knot_pattern = id_to_knot[k['id']]
    else:
        print("unknown pendant "+str(k[id]))
        return ""
    
    s = ""    
    instr = knot_to(knot_pattern, context, "nop")
    num_children = len(k["desc"])
    if num_children>0:
        child_context = context
        if instr in instr_to_context:
            child_context = instr_to_context[instr]
        s+=instr+"(["
        for i,ck in enumerate(k["desc"]):
            s+=pendant_to_python(ck, depth+1, child_context)
            if i<len(k["desc"])-1:
                s+=", "
        s+="])"
    else:
        s+=instr
    return s



def pendant_to_ascii(k, depth):
    if k['id'] in id_to_knot:
        knot_pattern = id_to_knot[k['id']]
    else:
        print("unknown pendant "+str(k[id]))
        return ""
    
    s = ""
    for i in range(0,depth):
        s+="    "
    s += knot_pattern+"("+str(k['id'])+")\n"    
    num_children = len(k["desc"])
    if num_children>0:
        for i,k in enumerate(k["desc"]):            
            s += pendant_to_ascii(k, depth+1)
    
    return s


def sequence_pattern(servo, p1, p2, p3, p4):    
    code1 = ord(p1)<<8 | ord(p2)
    code2 = ord(p3)<<8 | ord(p4)
    return "(set! reg-pattern-"+servo+"-1 "+str(code1)+")\n"+\
        "(set! reg-pattern-"+servo+"-2 "+str(code2)+")\n"

def sequence_pattern_front(p1, p2, p3, p4):
    if len(arr)==4:
        return sequence_pattern("front", arr[0], arr[1], arr[2], arr[3])
    else: return ""

def sequence_pattern_middle(arr):
    if len(arr)==4:
        return sequence_pattern("middle", arr[0], arr[1], arr[2], arr[3])
    else: return ""
    
def sequence_pattern_back(p1, p2, p3, p4):
    if len(arr)==4:
        return sequence_pattern("back", arr[0], arr[1], arr[2], arr[3])
    else: return ""


############################################################################

kl = {}

transmitter = transmit.transmit()
last_code = ""

# while True:
#     if transmitter.program(7,"((forever (set! next-pattern walk-stop)))"):
#         print("success")
#     else:
#         print("fail")
#     transmitter.save_eeprom(7)
#     time.sleep(1)

# while True:

#     code = sequence_pattern("middle","a","0","0","0")
    
#     if transmitter.program(7,"((forever "+code+"))"):
#         print("success")
#     else:
#         print("fail")
    
#     time.sleep(5)
        
while True:
    if s.in_waiting:
        st = s.readline()
        #st = str(st).replace("\'", "\"").rstrip()[2:-5]
        st = str(st).replace("\'", "\"").rstrip()
        #print(st)
        code = last_code

        try:
            k = json.loads(st)
        
            kl[k[0]]=k[1]
            code = pendant_to_python(k[1],0,knot_to_instr)
            print(code)
            robot_code = eval(code) 
      
        except:
            print("failed to read properly")
            continue

        if code!=last_code:
            print(pendant_to_ascii(k[1],0))
            print(robot_code)        

            if transmitter.program(7,"((forever "+robot_code+"))"):
                print("success")
            else:
                print("fail")
            last_code=code

        
    time.sleep(0.1)
