import time
import rtmidi_python as rtmidi
import random
import sys
import platform
import copy
import os

import serial
import json


import os
from queue import Queue
from threading import Thread

s = serial.Serial("/dev/ttyACM0", 115200)

id_to_knot = {
    1: "-/-",
    2: "-/-",
    3: "-//-",
    4: "-/-",
    5: "-/-/-/-",
    6: "-///-",
    9: "-////-",
    16: "-/--//-",
    17: "-/--///-",
    18: "-//-",
    19: "-//-",
}


knot_to_note = {
        "-/-": 30,
        "-//-": 35,
        "-///-": 40,
        "-////-": 42,
        "-/--///-": 45,        
}

natural_minor = [2, 1, 2, 2, 1, 2, 2]
harmonic_minor  = [2, 1, 2, 2, 1, 3, 1]
melodic_minor = [2, 1, 2, 2, 2, 2, 1]

midiout = rtmidi.MidiOut(b"out")
midiout.open_port(1)

def scaleify(n,sc):
        o=0
        p=0
        while o<127 and p<n:
                o+=sc[p%len(sc)]
                p+=1
        return int(o)

def note_on(channel,pitch,vol):
        print([channel,pitch,vol])
        midiout.send_message([0xb0|channel,0x7b,0x00])
        midiout.send_message([0x90|channel,pitch,vol])

def knot_to(k, lookup, default):
    if not k in lookup:
        return default
    else:
        return lookup[k]

def pendant_to_sequence(k, depth):
    if k['id'] in id_to_knot:
        knot_pattern = id_to_knot[k['id']]
    else:
        print("unknown pendant "+str(k[id]))
        return ""
    
    note = knot_to(knot_pattern, knot_to_note, 20)
    s = [[note,1/depth]]    

    num_children = len(k["desc"])
    if num_children>0:
        for i,k in enumerate(k["desc"]):
                s+=pendant_to_sequence(k, depth+1)

    return s

seq = [[30, 1.5]]
seq_pos = 0

class NoiseWorker(Thread):
        
    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue
        self.seq_pos = 0

    def run(self):
        while True:
                if self.seq_pos>=len(seq):
                        self.seq_pos=0
                n = seq[self.seq_pos]
                self.seq_pos+=1                
                note_on(2,n[0]*2,127)
                time.sleep(n[1]/2.0)

queue = Queue()
worker = NoiseWorker(queue)                
worker.start()

while True:
    if s.in_waiting:
        st = s.readline()
        st = str(st).replace("\'", "\"").rstrip()[2:-5]
        #print(st)
        try:
            k = json.loads(st)
        except:
            print("failed to read properly")
        #kl[k[0]]=k[1]

        seq=pendant_to_sequence(k[1],1)
        print(seq)

        
    time.sleep(0.1)



