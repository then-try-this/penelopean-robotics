import serial
import time
import json

s = serial.Serial("/dev/ttyACM0", 115200)

id_to_knot = {
    1: "-/-",
    2: "b",
    3: "-///-",
    4: "-/-",
    5: "-/--",
    6: "-//-",
    8: "-//-",
    9: "-////-",
    16: "-/--//-",
    17: "fb",
    18: "m",
    19: "-//-",
    21: "-////-",
}

def pendant_to_ascii(k, depth):
    if k['id'] in id_to_knot:
        knot_pattern = id_to_knot[k['id']]
    else:
        print("unknown pendant "+str(k['id']))
        return ""
    
    s = ""
    for i in range(0,depth):
        s+="    "
    s += knot_pattern+"("+str(k['id'])+")\n"    
    num_children = len(k["desc"])
    if num_children>0:
        for i,k in enumerate(k["desc"]):            
            s += pendant_to_ascii(k, depth+1)
    
    return s

last_ascii = ""

while True:
    if s.in_waiting:
        st = s.readline()
        st = str(st).replace("\'", "\"").rstrip()[2:-5]
        k = json.loads(st)
        ##print(k)
        asc = pendant_to_ascii(k[1],0)
        if last_ascii!=asc:
            last_ascii=asc
            print(asc)
        
    time.sleep(0.1)
