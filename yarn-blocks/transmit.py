from __future__ import print_function
import sys
sys.path.append("../pi")

import os
RUNNING_ON_PI = os.uname()[4][:3] == 'arm' 
if RUNNING_ON_PI: import radio
if RUNNING_ON_PI: import robot
import yarnasm
import time
import threading
import sys 
import math
import subprocess
import binascii
import sys
import threading
from time import sleep
import thread
import psutil
import logging

def quit_function(fn_name):
    # print to stderr, unbuffered in Python 2.
    print('{0} took too long'.format(fn_name), file=sys.stderr)
    sys.stderr.flush() # Python 3 stderr is likely buffered.
    thread.interrupt_main() # raises KeyboardInterrupt
    restart_program()

def restart_program():
    """Restarts the current program, with file objects and descriptors
       cleanup
    """
    try:
        p = psutil.Process(os.getpid())
        for handler in p.open_files() + p.connections():
            os.close(handler.fd)
    except e:
        logging.error(e)

    print("restarting")
    python = sys.executable
    os.execl(python, python, *sys.argv)

def exit_after(s):
    '''
    use as decorator to exit process if 
    function takes longer than s seconds
    '''
    def outer(fn):
        def inner(*args, **kwargs):
            timer = threading.Timer(s, quit_function, args=[fn.__name__])
            timer.start()
            try:
                result = fn(*args, **kwargs)
            finally:
                timer.cancel()
            return result
        return inner
    return outer

class transmit:
    def __init__(self):
        if RUNNING_ON_PI:
            self.radio = radio.radio([0xa7, 0xa7, 0xa7, 0xa7, 0xaa])
            self.swarm = [robot.robot([0xa7, 0xa7, 0xa7, 0xa7, 0x01]),
                          robot.robot([0xa7, 0xa7, 0xa7, 0xa7, 0x02]),
                          robot.robot([0xa7, 0xa7, 0xa7, 0xa7, 0x03]),
                          robot.robot([0xa7, 0xa7, 0xa7, 0xa7, 0x04]),
                          robot.robot([0xa7, 0xa7, 0xa7, 0xa7, 0x05]),
                          robot.robot([0xa7, 0xa7, 0xa7, 0xa7, 0x06]),
                          robot.robot([0xa7, 0xa7, 0xa7, 0xa7, 0x07]),
                          robot.robot([0xa7, 0xa7, 0xa7, 0xa7, 0x08])
            ]


        self.compiler = yarnasm.compiler()
        self.lisp_fifo_name = "../fifo/lisp_fifo"
        self.asm_fifo_name = "../fifo/asm_fifo"
        os.remove(self.lisp_fifo_name)
        os.remove(self.asm_fifo_name)
        os.mkfifo(self.lisp_fifo_name)
        os.mkfifo(self.asm_fifo_name)
        # start compiler
        subprocess.Popen(['racket', '../compiler/yarnc.scm'])

    @exit_after(5)
    def program(self,robot_id,lisp_file):
        
        asm = ""
        fifo=open(self.lisp_fifo_name,"w")
        fifo.write(lisp_file)
        fifo.close()

        fifo=open(self.asm_fifo_name,"r")
        for line in fifo.readlines():
            asm+=line
        fifo.close()
        
        #print(asm)
        print(binascii.hexlify(self.compiler.assemble_bytes(asm)))
        
        if RUNNING_ON_PI:
            if self.swarm[robot_id].send_asm(asm,self.compiler,self.radio) == False:
                return False
        return True

    def save_eeprom(self,robot_id):
        if RUNNING_ON_PI: self.swarm[robot_id].save_eeprom(self.radio)



    
